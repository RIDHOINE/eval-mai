<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    <a class="navbar-brand" href="#">Home</a>
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="navbar-brand" href="#">Projets</a>
      </li>
      <li class="nav-item">
      <a class="navbar-brand" href="#">Articles</a>
      </li>
    </ul>
    <div class="my-2 my-lg-0">
                             <button type="button" class="btn btn-primary">Messagerie</button> {{--Ce boutton est disponible seulement dans le cas l'utilisateur est connecté --}}
                             <button type="button" class="btn btn-success">Connexion</button>  {{--Ce boutton est disponible seulement dans le cas aucune session n'est demarer (un visiteur)--}}
                             <button type="button" class="btn btn-danger">Deconnexion</button> {{--Ce boutton est disponible seulement dans le cas l'utilisateur est connecté --}}
    </div>
  </div>
</nav>


{{-- Affichage de l'aperçu--}}


<div class="d-flex flex-column align-items-center">
  <div class="p-2">
                  <h4>{{$titre->title}}</h4>
  </div>
  <div class="p-2">
                  <div class="card" style="width: 60rem;">
  <img class="card-img-top .w75 .h-auto" src="https://picsum.photos/50/?blur=2" alt="Card image cap">
                 <div class="card-body">
                 <p class="card-text">Description is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                 <a href="#" class="btn btn-warning">Modifier</a> 
                 <a href="#" class="btn btn-danger">Supprimer</a> 
  </div>
</div>
 













</body>
</html>

